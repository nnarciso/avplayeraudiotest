//
//  CBCStreamManager.m
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2016-01-13.
//  Copyright © 2016 Canadian Broadcast Corporation. All rights reserved.
//

#import "CBCStreamManager.h"

@interface CBCStreamManager ()

@property (nonatomic, strong) NSMutableArray<CBCStream *> *streams;

@end

@implementation CBCStreamManager

+ (CBCStreamManager *)sharedManager {
    static CBCStreamManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[CBCStreamManager alloc] init];
    });
    return _sharedManager;
}

- (NSArray<CBCStream *> *)getStreams
{
    if (self.streams == nil)
    {
        [self initStreams];
    }
    return self.streams;
}

- (void)initStreams
{
    self.streams = [[NSMutableArray alloc] init];
    
    [self makeStream:@"New 1 - Radio 1 Waterloo" urlString:@"http://liveradio.cbc.ca/i/CBCR1_EKW@6742/master.m3u8"];
    [self makeStream:@"New 2 - Radio 1 Kamloops" urlString:@"http://liveradio.cbc.ca/i/CBCR1_KAM@66233/master.m3u8"];
    [self makeStream:@"New 3 - Radio 1 Saskatoon" urlString:@"http://liveradio.cbc.ca/i/CBCR1_SSK@66110/master.m3u8"];
    [self makeStream:@"New 4 - Radio 3" urlString:@"http://liveradio.cbc.ca/i/CBCR3_WEB@75878/master.m3u8"];
    [self makeStream:@"New Bad Stream" urlString:@"http://cbctestradiolive-lh.akamaihd.net/i/cbcradiolivetest1_9@336443/master.m3u8"];
    [self makeStream:@"New Staging" urlString:@"http://cbctestradiolive-lh.akamaihd-staging.net/i/CBC_ADLTAL@66530/master.m3u8"];
    [self makeStream:@"Old Radio 1 Toronto" urlString:@"http://playerservices.streamtheworld.com/pls/CBC_R1_TOR_H.pls"];
    [self makeStream:@"Old Radio 2 Toronto" urlString:@"http://playerservices.streamtheworld.com/pls/CBC_R2_TOR_H.pls"];
    [self makeStream:@"Old Radio 2 Vancouver" urlString:@"http://playerservices.streamtheworld.com/pls/CBC_R2_VCR_H.pls"];
    [self makeStream:@"Old Sonica" urlString:@"http://playerservices.streamtheworld.com/pls/CBC_SONICA_H.pls"];
    [self makeStream:@"On Demand Clip 0:43" urlString:@"http://thumbnails.cbc.ca/maven_legacy/thumbnails/11/913/irrelevantshow_20151221_17796_uploaded.mp3"];
    [self makeStream:@"On Demand Clip 2:02" urlString:@"http://thumbnails.cbc.ca/maven_legacy/thumbnails/15/897/irrelshowsketches_20160109_67445_uploaded.mp3"];
    [self makeStream:@"On Demand Clip 15:21" urlString:@"http://thumbnails.cbc.ca/maven_legacy/thumbnails/15/337/breakawaymtl_20150121_50965_uploaded.mp3"];
    [self makeStream:@"On Demand Clip 55:21" urlString:@"http://thumbnails.cbc.ca/maven_legacy/thumbnails/15/577/quirksaio_20160109_70462_uploaded.mp3"];
    [self makeStream:@"On Demand Clip 1:42:00" urlString:@"http://thumbnails.cbc.ca/maven_legacy/thumbnails/14/225/sundayedition_20151224_60933_uploaded.mp3"];
    [self makeStream:@"Apple Test Stream" urlString:@"https://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8"];
}

- (void)makeStream:(NSString *)name urlString:(NSString *)urlString
{
    CBCStream *stream = [[CBCStream alloc] initWithName:name urlString:urlString];
    [self.streams addObject:stream];
}

@end
