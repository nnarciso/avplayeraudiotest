//
//  main.m
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2015-12-02.
//  Copyright © 2015 Canadian Broadcast Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
