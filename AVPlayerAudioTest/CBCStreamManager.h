//
//  CBCStreamManager.h
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2016-01-13.
//  Copyright © 2016 Canadian Broadcast Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CBCStream.h"

@interface CBCStreamManager : NSObject

+ (CBCStreamManager *)sharedManager;

- (NSArray<CBCStream *> *)getStreams;

@end
