//
//  CBCStream.h
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2016-01-04.
//  Copyright © 2016 Canadian Broadcast Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBCStream : NSObject 

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL *url;

- (instancetype)initWithName:(NSString *)name urlString:(NSString *)urlString;

@end
