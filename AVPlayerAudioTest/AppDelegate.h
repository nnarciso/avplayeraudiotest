//
//  AppDelegate.h
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2015-12-02.
//  Copyright © 2015 Canadian Broadcast Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

