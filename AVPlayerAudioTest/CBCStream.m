//
//  CBCStream.m
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2016-01-04.
//  Copyright © 2016 Canadian Broadcast Corporation. All rights reserved.
//

#import "CBCStream.h"

@implementation CBCStream

- (instancetype)initWithName:(NSString *)name urlString:(NSString *)urlString
{
    self = [super init];
    if (self != nil)
    {
        self.name = name;
        self.url = [NSURL URLWithString:urlString];
    }
    return self;
}

@end
