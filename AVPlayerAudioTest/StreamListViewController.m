//
//  StreamListViewController.m
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2015-12-08.
//  Copyright © 2015 Canadian Broadcast Corporation. All rights reserved.
//

#import "StreamListViewController.h"
#import "StreamTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import "CBCStream.h"
#import "CBCStreamManager.h"

//TODO: Extract all the AV code to a new class leaving only View Controller related code in this class

@interface StreamListViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *logTextView;

@property (strong, nonatomic) NSMutableArray<CBCStream *> *streams;
@property (strong, nonatomic) AVPlayer *player;

@property (strong, nonatomic) AVAsset *asset;
@property (nonatomic) BOOL isLocked;
@property (weak, nonatomic) IBOutlet UIButton *lockButton;

@end


@implementation StreamListViewController

static void *AVPlayerStatusObserverContext = &AVPlayerStatusObserverContext;
static void *AVPlayerRateObserverContext = &AVPlayerRateObserverContext;

static void *AVPlayerItemStatusObserverContext = &AVPlayerItemStatusObserverContext;
static void *AVPlayerItemPlaybackBufferEmptyObserverContext = &AVPlayerItemPlaybackBufferEmptyObserverContext;
static void *AVPlayerItemPlaybackLikelyToKeepUpObserverContext = &AVPlayerItemPlaybackLikelyToKeepUpObserverContext;
static void *AVPlayerItemTimedMetadataObserverContext = &AVPlayerItemTimedMetadataObserverContext;
static void *AVPlayerItemLoadedTimeRangesObserverContext = &AVPlayerItemLoadedTimeRangesObserverContext;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self initAudioSession];
    [self initPlayer];
    
    self.streams = [NSMutableArray arrayWithArray:[[CBCStreamManager sharedManager] getStreams]];

    self.tableView.estimatedRowHeight = 44.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.logTextView.text = @"";
 
    //Two finger tap to display additional debug information
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAdditionalDebugInformationHandler:)];
    tapGestureRecognizer.numberOfTouchesRequired = 2;
    [self.logTextView addGestureRecognizer:tapGestureRecognizer];
    
    self.view.backgroundColor = [UIColor blueColor];
}


#pragma mark - Audio Session

- (void)initAudioSession
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    NSError *audioSessionError = nil;
    [session setActive:YES error:&audioSessionError];
    if (audioSessionError) {
        NSLog(@"Error %ld, %@", (long)audioSessionError.code, audioSessionError.localizedDescription);
    }
    
    [self addObserversForAudioSession];
}

- (void)addObserversForAudioSession
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioSessionInterruption:)
                                                 name:AVAudioSessionInterruptionNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioSessionRouteChange:)
                                                 name:AVAudioSessionRouteChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioSessionMediaServicesWereReset)
                                                 name:AVAudioSessionMediaServicesWereResetNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioSessionMediaServicesWereLost)
                                                 name:AVAudioSessionMediaServicesWereLostNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(audioSessionSilenceSecondaryAudioHint)
                                                 name:AVAudioSessionSilenceSecondaryAudioHintNotification
                                               object:nil];
}

- (void)audioSessionInterruption:(NSNotification *)notification
{
    [self logDebug:@"audioSessionInterruption called"];
}

- (void)audioSessionRouteChange:(NSNotification *)notification
{
    [self logDebug:@"audioSessionRouteChange called"];
}

- (void)audioSessionMediaServicesWereReset
{
    [self logDebug:@"audioSessionMediaServicesWereReset called"];
}

- (void)audioSessionMediaServicesWereLost {
    [self logDebug:@"audioSessionMediaServicesWereLost called"];
}

- (void)audioSessionSilenceSecondaryAudioHint {
    [self logDebug:@"audioSessionSilenceSecondaryAudioHint called"];
}


#pragma mark - Player Initialization

- (void)initPlayer
{
    self.player = [[AVPlayer alloc] init];
    [self addObserversForPlayer];
}

- (void)addObserversForPlayer
{
    [self.player addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerStatusObserverContext];
    [self.player addObserver:self forKeyPath:@"rate" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerRateObserverContext];
}

- (void)removeObserversForPlayer
{
    [self.player removeObserver:self forKeyPath:@"status"];
    [self.player removeObserver:self forKeyPath:@"rate"];
}


#pragma mark - PlayerItem

- (void)addObserversForPlayerItem:(AVPlayerItem *)playerItem
{
    [playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerItemStatusObserverContext];
    [playerItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerItemPlaybackBufferEmptyObserverContext];
    [playerItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerItemPlaybackLikelyToKeepUpObserverContext];
    //[playerItem addObserver:self forKeyPath:@"timedMetadata" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerItemTimedMetadataObserverContext];
    [playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:AVPlayerItemLoadedTimeRangesObserverContext];

    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidPlayToEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemFailedToPlayToEndTime:)
                                                 name:AVPlayerItemFailedToPlayToEndTimeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemPlaybackStalled:)
                                                 name:AVPlayerItemPlaybackStalledNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemNewErrorLogEntry:)
                                                 name:AVPlayerItemNewErrorLogEntryNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemNewAccessLogEntry:)
                                                 name:AVPlayerItemNewAccessLogEntryNotification
                                               object:nil];
}

- (void)removeObserversForPlayerItem:(AVPlayerItem *)playerItem
{
    [playerItem removeObserver:self forKeyPath:@"status"];
    [playerItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
    [playerItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    //[playerItem removeObserver:self forKeyPath:@"timedMetadata"];
    [playerItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemFailedToPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemPlaybackStalledNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemNewErrorLogEntryNotification object:nil];    
}


#pragma mark - Observation Handlers

- (void)observeValueForKeyPath:(NSString*)keyPath ofObject:(id)object change:(NSDictionary*)change context:(void*)context {
    AVPlayer *player;
    if ([object isKindOfClass:[AVPlayer class]])
    {
        player = (AVPlayer *)object;
    }

    AVPlayerItem *playerItem;
    if ([object isKindOfClass:[AVPlayerItem class]])
    {
        playerItem = (AVPlayerItem *)object;
    }
    
    //AVPlayer changes
    if (context == AVPlayerStatusObserverContext)
    {
        [self logDebug:@"Player Status: %@", [self getPlayerStatusAsStringForPlayer:player]];

//        [self.player seekToTime:CMTimeMakeWithSeconds(80, NSEC_PER_SEC) completionHandler:^(BOOL finished) {
//            [self logDebug:@"Finished: %@", finished ? @"YES" : @"NO"];
//        }];

        return;
    }
    
    if (context == AVPlayerRateObserverContext)
    {
        [self logDebug:@"Player Rate: %f", player.rate];
        [self syncUI];
        return;
    }
    
    //AVPlayerItem Changes
    if (context == AVPlayerItemStatusObserverContext)
    {
        [self logDebug:@"Player Item Status: %@", [self getPlayerItemStatusAsStringForPlayerItem:playerItem]];
        [self syncUI];
        return;
    }
    
    if (context == AVPlayerItemPlaybackBufferEmptyObserverContext)
    {
        [self logDebug:@"Playback buffer empty: %@", playerItem.playbackBufferEmpty ? @"YES" : @"NO"];
        return;
    }

    if (context == AVPlayerItemPlaybackLikelyToKeepUpObserverContext)
    {
        AVPlayerItem *playerItem = (AVPlayerItem *)object;
        [self logDebug:@"Playback likely to keep up: %@", playerItem.playbackLikelyToKeepUp ? @"YES" : @"NO"];
        [self syncUI];
        return;
    }
    
    if (context == AVPlayerItemTimedMetadataObserverContext)
    {
        for (AVMetadataItem* metadata in playerItem.timedMetadata)
        {
            [self logDebug:@"\nkey: %@\nkeySpace: %@\ncommonKey: %@\nvalue: %@", [metadata.key description], metadata.keySpace, metadata.commonKey, metadata.stringValue];
            if ([metadata.commonKey isEqualToString:@"artwork"])
            {
                //self.imageView.image = [UIImage imageWithData:(NSData *)metadata.value];
            }
        }
        return;
    }
    
    if (context == AVPlayerItemLoadedTimeRangesObserverContext) {
        if (change[NSKeyValueChangeNewKey] == nil || change[NSKeyValueChangeNewKey] == [NSNull null] || ![change[NSKeyValueChangeNewKey] isKindOfClass:[NSArray class]])
        {
            return;
        }
        
        NSArray *timeRanges = (NSArray*)[change objectForKey:NSKeyValueChangeNewKey];
//        if ([timeRanges count] > 0) {
//            CMTimeRange timeRange=[[timeRanges objectAtIndex:0]CMTimeRangeValue];
//            NSString *startTime = [self getTimeIntervalString:CMTimeGetSeconds(timeRange.start)];
//            NSString *duration = [self getTimeIntervalString:CMTimeGetSeconds(timeRange.duration)];
//            CMTime currentTime = self.player.currentItem.currentTime;
//            NSString *currentTimeString = [self getTimeIntervalString:CMTimeGetSeconds(currentTime)];
//            [self logDebug:@"loaded time range start:%@ duration:%@ currentTime:%@", startTime, duration, currentTimeString];
//        }
        
        if ([timeRanges count] > 1)
        {
            NSLog(@"Multiple time ranges: %ld", (long)[timeRanges count]);
        }
        
        [self syncUI];

        return;
    }
    
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

- (void)syncUI
{
    UIColor *backgroundColor;
    if ([self isPlaying])
    {
        backgroundColor = [UIColor redColor];
        //[self logDebug:@"##############################################"];
    }
    else
    {
        backgroundColor = [UIColor blueColor];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.view.backgroundColor = backgroundColor;
        [self.view setNeedsDisplay];
    });
}

- (BOOL)isPlaying
{
    if (self.player == nil ||
        self.player.rate == 0 ||
        self.player.currentItem == nil ||
        self.player.currentItem.status != AVPlayerStatusReadyToPlay)
    {
        return NO;
    }
        
    
    if ([self isLiveStream])
    {
        return self.player.currentItem.playbackLikelyToKeepUp;
    }
    else
    {
        if ([self.player.currentItem.loadedTimeRanges count] < 1)
        {
            return  NO;
        }
        
        NSArray *timeRanges = self.player.currentItem.loadedTimeRanges;
        for (NSValue *timeRangeValue in timeRanges)
        {
            CMTimeRange timeRange = [timeRangeValue CMTimeRangeValue];
            CMTime currentTime = self.player.currentItem.currentTime;
            BOOL withinLoadedTimeRange = CMTimeGetSeconds(timeRange.duration) >= 1.0 && CMTimeRangeContainsTime(timeRange, currentTime);
            if (withinLoadedTimeRange)
            {
                return YES;
            }
        }
    }
    
    return NO;
}

- (BOOL)isLiveStream
{
    if (self.player == nil || self.player.currentItem == nil || self.player.currentItem.asset == nil)
    {
        return NO;
    }
    
    AVAsset *currentPlayerAsset = self.player.currentItem.asset;
    if (![currentPlayerAsset isKindOfClass:[AVURLAsset class]])
    {
        return NO;
    }

    NSURL *url = [(AVURLAsset *)currentPlayerAsset URL];
    return [[url pathExtension] isEqualToString:@"m3u8"];
}


- (void)playerItemDidPlayToEnd:(NSNotification *)notification {
    [self logDebug:@"DidPlayToEnd"];
}

- (void)playerItemFailedToPlayToEndTime:(NSNotification *)notification {
    NSError *error = notification.userInfo[AVPlayerItemFailedToPlayToEndTimeErrorKey];
    [self logDebug:@"FailedToPlayToEndTime: %@", error.localizedDescription];
}

- (void)playerItemPlaybackStalled:(NSNotification *)notification {
    [self logDebug:@"Stalled"];
}

- (void)playerItemNewErrorLogEntry:(NSNotification *)notification {
    AVPlayerItemErrorLogEvent *event = [self.player.currentItem.errorLog.events lastObject];
    [self logDebug:@"New Error Log Entry: %@", [self getErrorLogEventAsString:event]];
}

- (void)playerItemNewAccessLogEntry:(NSNotification *)notification {
    AVPlayerItemAccessLogEvent *event = [self.player.currentItem.accessLog.events lastObject];
    [self logDebug:@"New Access Log Entry: %@", [self getAccessLogEventAsString:event]];
}


#pragma mark - playUrl

- (void)playUrl:(NSURL *)url
{
    [self logDebug:@"Attempting to play URL: %@", [url absoluteString]];
    
    if (self.player.currentItem != nil)
    {
        [self removeObserversForPlayerItem:self.player.currentItem];
    }
    
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:url];
    [self addObserversForPlayerItem:playerItem];
    [self.player replaceCurrentItemWithPlayerItem:playerItem];
    
    [self.player play];
}


#pragma mark - Debugging Methods

- (void)logDebug:(NSString *)format, ...
{
    NSString* timestamp = [self getTimeStampFromDate:[NSDate date]];
    
    va_list vargs;
    va_start(vargs, format);
    NSString* formattedMessage = [[NSString alloc] initWithFormat:format arguments:vargs];
    va_end(vargs);
    
    NSString* message = [NSString stringWithFormat:@"%@ %@", timestamp, formattedMessage];
    NSLog(@"%@", message);
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self.logTextView.text = [NSString stringWithFormat:@"%@%@\n", self.logTextView.text, message];
//        [self.logTextView scrollRangeToVisible:NSMakeRange(self.logTextView.text.length-2, 0)];
//        [self.logTextView setScrollEnabled:NO];
//        [self.logTextView setScrollEnabled:YES];
//    });
}

- (void)showAdditionalDebugInformation
{
    [self logDebug:@"<Additional Debug Information Start>"];
    [self logDebug:@"Sample Rate: %f", [[AVAudioSession sharedInstance] sampleRate]];
    [self logDebug:@"Player Status: %@", [self getPlayerStatusAsStringForPlayer:self.player]];
    [self logDebug:@"Player Item status: %@", [self getPlayerItemStatusAsStringForPlayerItem:self.player.currentItem]];
    [self logDebug:@"Player Rate: %f", self.player.rate];
    
    [self logDebug:@"Bytes Transferred: %ld", (long)[self.player.currentItem.accessLog.events lastObject].numberOfBytesTransferred];
    
    NSString *timeString = [self getTimeIntervalString:CMTimeGetSeconds(self.player.currentTime)];
    NSString *durationString = [self getTimeIntervalString:CMTimeGetSeconds(self.player.currentItem.duration)];
    [self logDebug:@"Player Current Time: %@ Player Item Duration: %@", timeString, durationString];
    
    [self logDebug:@"Loaded Time Ranges: %@", [self getLoadedTimeRangesAsString]];
    
    [self logDebug:@"Buffer duration: %f", [[AVAudioSession sharedInstance] IOBufferDuration]];
    [self logDebug:@"Error Log:\n%@", [self getErrorLogAsString]];
    [self logDebug:@"Access Log:\n%@", [self getAccessLogAsString]];
    
    [self logDebug:@"<Additional Debug Information End>"];
}


#pragma mark - Helper methods

- (NSString *)getTimeStampFromDate:(NSDate *)date
{
    static NSDateFormatter* timeStampFormat;
    if (timeStampFormat == nil)
    {
        timeStampFormat = [[NSDateFormatter alloc] init];
        [timeStampFormat setDateFormat:@"HH:mm:ss.SSS"];
        [timeStampFormat setTimeZone:[NSTimeZone systemTimeZone]];
    }
    
    return [timeStampFormat stringFromDate:date];
}

- (NSString *)getBitrateString:(double)bitrate
{
    static NSNumberFormatter *bitrateFormatter;
    if (bitrateFormatter == nil)
    {
        bitrateFormatter = [[NSNumberFormatter alloc] init];
        [bitrateFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
        bitrateFormatter.maximumFractionDigits = 0;
    }
   return [bitrateFormatter stringFromNumber:[NSNumber numberWithDouble:bitrate]];
}

- (NSString *)getTimeIntervalString:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hours, (long)minutes, (long)seconds];
}

- (NSString *)getErrorLogEventAsString:(AVPlayerItemErrorLogEvent *)event
{
    NSString* timestamp = [self getTimeStampFromDate:event.date];
    return [NSString stringWithFormat:@"%@ %@ [%ld] - %@", timestamp, event.errorDomain, (long)event.errorStatusCode, event.errorComment];
}

- (NSString *)getAccessLogEventAsString:(AVPlayerItemAccessLogEvent *)event
{
    NSString *indicatedBitrate = [self getBitrateString:event.indicatedBitrate];
    NSString *observedBitrate = [self getBitrateString:event.observedBitrate];
    return [NSString stringWithFormat:@"Indicated Bitrate:%@ Observed Bitrate:%@ Stalls:%ld", indicatedBitrate, observedBitrate , (long)event.numberOfStalls];
}

- (NSString *)getErrorLogAsString
{
    NSMutableArray *eventStrings = [[NSMutableArray alloc] init];
    for (AVPlayerItemErrorLogEvent *event in self.player.currentItem.errorLog.events)
    {
        [eventStrings addObject:[self getErrorLogEventAsString:event]];
    }
    return [eventStrings componentsJoinedByString:@"\n"];
}

- (NSString *)getAccessLogAsString
{
    NSMutableArray *eventStrings = [[NSMutableArray alloc] init];
    for (AVPlayerItemAccessLogEvent *event in self.player.currentItem.accessLog.events)
    {
        [eventStrings addObject:[self getAccessLogEventAsString:event]];
    }
    return [eventStrings componentsJoinedByString:@"\n"];
}

- (NSString *)getPlayerStatusAsStringForPlayer:(AVPlayer *)player
{
    if (player.status == AVPlayerStatusFailed)
    {
        return [NSString stringWithFormat:@"failed: %@", [player.error localizedDescription]];
    }
    if (player.status == AVPlayerStatusReadyToPlay)
    {
        return @"ready to play";
    }
    if (player.status == AVPlayerStatusUnknown)
    {
        return @"unknown";
    }
    
    return @"Unknown AVPlayerStatus value";
}

- (NSString *)getPlayerItemStatusAsStringForPlayerItem:(AVPlayerItem *)playerItem
{
    if (playerItem.status == AVPlayerItemStatusFailed)
    {
        return [NSString stringWithFormat:@"failed: %@", [playerItem.error localizedDescription]];
    }
    if (playerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        return @"ready to play";
    }
    if (playerItem.status == AVPlayerItemStatusUnknown)
    {
        return @"unknown";
    }
    
    return @"Unknown AVPlayerItemStatus value";
}

- (NSString *)getLoadedTimeRangesAsString
{
    CMTimeRange timeRange=[[self.player.currentItem.loadedTimeRanges firstObject] CMTimeRangeValue];
    NSString *startTime = [self getTimeIntervalString:CMTimeGetSeconds(timeRange.start)];
    NSString *duration = [self getTimeIntervalString:CMTimeGetSeconds(timeRange.duration)];
    return [NSString stringWithFormat:@"start:%@ duration:%@", startTime, duration];
}


#pragma mark - Button Press handlers

- (IBAction)pausePressed:(id)sender {
    [self logDebug:@"Pause pressed"];
    [self.player pause];
}

- (IBAction)playPressed:(id)sender {
    [self logDebug:@"Play pressed"];
    [self.player play];
}

- (IBAction)addPressed:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add Stream" message:@"Enter Stream Name and URL. Note the stream will be removed when the app closes." preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Name";
    }];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"URL";
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *name = alert.textFields[0].text;
        NSString *urlString = alert.textFields[1].text;
        CBCStream *stream = [[CBCStream alloc] initWithName:name urlString:urlString];
        [self.streams addObject:stream];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.streams count] -1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        });
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)lockPressed:(id)sender {
    self.isLocked = !self.isLocked;
}

- (void)setIsLocked:(BOOL)isLocked
{
    _isLocked = isLocked;
    [self.lockButton setTitle:isLocked ? @"L" : @"U" forState:UIControlStateNormal];
    self.tableView.userInteractionEnabled = !isLocked;
}

#pragma mark - Tap Gesture Handlers

- (void)showAdditionalDebugInformationHandler:(id)sender
{
    [self showAdditionalDebugInformation];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.streams count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    StreamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell"];
    CBCStream *stream = self.streams[indexPath.row];
    [cell configureCell:stream];
    
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBCStream *stream = self.streams[indexPath.row];
    [self playUrl:stream.url];
}


- (void)dealloc
{
    if (self.player.currentItem != nil)
    {
        [self removeObserversForPlayerItem:self.player.currentItem];
    }
    [self removeObserversForPlayer];
}

@end
