//
//  StreamTableViewCell.h
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2015-12-08.
//  Copyright © 2015 Canadian Broadcast Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBCStream.h"

@interface StreamTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *urlLabel;

- (void)configureCell:(CBCStream *)stream;

@end
