//
//  StreamTableViewCell.m
//  AVPlayerAudioTest
//
//  Created by Nelson Narciso on 2015-12-08.
//  Copyright © 2015 Canadian Broadcast Corporation. All rights reserved.
//

#import "StreamTableViewCell.h"

@implementation StreamTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)configureCell:(CBCStream *)stream
{
    self.nameLabel.text = stream.name;
    self.urlLabel.text = [stream.url absoluteString];
    
    //selected background color
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:164/255.0 green:205/255.0 blue:255/255.0 alpha:1.0];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
